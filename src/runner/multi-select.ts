/** Dependencies */
import {
    NodeBlock,
    Slots,
    Str,
    Value,
    each,
    filter,
    findFirst,
    isNumberFinite,
    map,
    reduce,
    validator,
} from "@tripetto/runner";
import { IOption } from "./option";

export abstract class MultiSelect extends NodeBlock<{
    options?: IOption[];
    min?: number;
    max?: number;
    required?: boolean;
    randomize?: boolean;
    formatSeparator?:
        | "comma"
        | "space"
        | "list"
        | "bullets"
        | "numbers"
        | "conjunction"
        | "disjunction"
        | "custom";
    formatSeparatorCustom?: string;
}> {
    /** Contains the randomized options order. */
    private randomized?: {
        readonly index: number;
        readonly id: string;
    }[];

    /** Contains the counter slot. */
    readonly counterSlot = this.valueOf<number, Slots.Number>(
        "counter",
        "meta"
    );

    /** Contains the concatenation slot. */
    readonly concatenationSlot = this.valueOf<string, Slots.Text>(
        "concatenation",
        "feature"
    );

    /** Contains the score slot. */
    readonly scoreSlot = this.valueOf<number, Slots.Numeric>(
        "score",
        "feature"
    );

    /** Retrieves if the multi-select dropdown is required. */
    get required(): boolean {
        return this.props.required || false;
    }

    private transform(): void {
        const options = filter(
            map(this.props.options, (option) => ({
                id: option.id,
                label: option.value || option.name || "",
                exclusive: option.exclusive,
                valueRef: this.valueOf<boolean>(option.id),
            })),
            (option) => option.valueRef?.value === true
        ).sort((a, b) => (b.valueRef?.time || 0) - (a.valueRef?.time || 0));
        const lastSelected = options.length && options[0];

        if (lastSelected) {
            options.forEach((option) => {
                if (
                    option.id !== lastSelected.id &&
                    (lastSelected.exclusive || option.exclusive)
                ) {
                    option.valueRef!.value = false;
                }
            });
        }

        if (this.props.max) {
            const max = this.props.max;
            let n = 0;

            options.forEach((option) => {
                if (option.valueRef?.value === true) {
                    n++;

                    if (n > max) {
                        option.valueRef!.value = false;
                    }
                }
            });
        }

        if (this.counterSlot) {
            const n = filter(
                options,
                (option) => option.valueRef?.value === true
            ).length;

            this.counterSlot.set(n);
        }

        if (this.concatenationSlot) {
            const list: string[] = [];
            let s = "";
            let n = 0;

            each(this.props.options, (option) => {
                const label = option.value || option.name || "";

                if (label && this.valueOf<boolean>(option.id)?.value === true) {
                    switch (this.props.formatSeparator) {
                        case "space":
                            s += (s === "" ? "" : " ") + label;
                            break;
                        case "list":
                            s += (s === "" ? "" : "\n") + label;
                            break;
                        case "bullets":
                            s += (s === "" ? "" : "\n") + "- " + label;
                            break;
                        case "numbers":
                            s += (s === "" ? "" : "\n") + `${++n}. ${label}`;
                            break;
                        case "conjunction":
                        case "disjunction":
                            list.push(label);
                            break;
                        case "custom":
                            s +=
                                (s === ""
                                    ? ""
                                    : this.props.formatSeparatorCustom || "") +
                                label;
                            break;
                        default:
                            s += (s === "" ? "" : ", ") + label;
                            break;
                    }
                }
            });

            if (
                this.props.formatSeparator === "conjunction" ||
                this.props.formatSeparator === "disjunction"
            ) {
                try {
                    const formatter = new Intl.ListFormat(
                        this.context.l10n.current || "en",
                        { type: this.props.formatSeparator }
                    );

                    s = formatter.format(list);
                } catch {
                    s = Str.iterateToString(list, ", ");
                }
            }

            this.concatenationSlot.set(s);
        }
    }

    private score(ref: IOption): void {
        if (this.scoreSlot && isNumberFinite(ref.score)) {
            this.scoreSlot.set(
                reduce(
                    this.props.options,
                    (score, option) =>
                        score +
                        ((this.valueOf(option.id)?.value === true &&
                            option.score) ||
                            0),
                    0
                )
            );
        }
    }

    /** Retrieves an option slot. */
    optionSlot(option: IOption): Value<boolean, Slots.Boolean> | undefined {
        return this.valueOf<boolean>(option.id, "dynamic", {
            confirm: "shallow",
            onChange: () => {
                this.transform();
                this.score(option);
            },
            onContext: (value, context) => {
                if (this.scoreSlot) {
                    context
                        .contextualValueOf(this.scoreSlot)
                        ?.set(
                            value.value === true ? option.score || 0 : undefined
                        );
                }
            },
        });
    }

    /** Retrieves if an option is selected. */
    isSelected(option: IOption): boolean {
        const optionSlot = this.optionSlot(option);

        return optionSlot?.value === true || false;
    }

    /** Sets the selected state of an option. */
    select(option: IOption, selected: boolean): boolean {
        const optionSlot = this.optionSlot(option);

        if (optionSlot) {
            optionSlot.value = selected;

            return optionSlot.value;
        }

        return false;
    }

    /** Toggles an option. */
    toggle(option: IOption): void {
        const optionSlot = this.optionSlot(option);

        if (optionSlot) {
            optionSlot.value = !optionSlot.value;
        }
    }

    /** Retrieves the options. */
    options<T>(props: {
        readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => T;
        readonly markdownifyToString: (md: string) => string;
    }): (Omit<IOption, "value" | "description"> & {
        readonly label: T;
        readonly description?: T;
        readonly value?: Value<boolean, Slots.Boolean>;
        readonly disabled: boolean;
    })[] {
        let selected = 0;
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const thisRef = this;
        const options =
            this.props.options?.map((option) => {
                const value = this.optionSlot(option);

                if (this.props.max && value?.value === true) {
                    selected++;
                }

                return {
                    id: option.id,
                    name: props.markdownifyToString(option.name),
                    label: props.markdownifyToJSX(option.name, false),
                    description:
                        (option.description &&
                            props.markdownifyToJSX(option.description, true)) ||
                        undefined,
                    value,
                    score: option.score,
                    exclusive: option.exclusive,
                    get disabled() {
                        return (
                            (thisRef.props.max &&
                                selected >= thisRef.props.max &&
                                value?.value !== true) ||
                            false
                        );
                    },
                };
            }) || [];

        if (this.props.randomize && options.length > 1) {
            if (
                !this.randomized ||
                this.randomized.length !== options.length ||
                findFirst(
                    this.randomized,
                    (option) => options[option.index]?.id !== option.id
                )
            ) {
                this.randomized = options.map((option, index) => ({
                    index,
                    id: option.id,
                }));

                let length = this.randomized.length;

                while (--length) {
                    const index = Math.floor(Math.random() * length);
                    const temp = this.randomized[length];

                    this.randomized[length] = this.randomized[index];
                    this.randomized[index] = temp;
                }
            }

            return this.randomized.map((option) => options[option.index]);
        } else if (this.randomized) {
            this.randomized = undefined;
        }

        return options;
    }

    @validator
    validate(): boolean {
        if (this.props.min || this.props.max) {
            const n = filter(
                this.props.options,
                (option) => this.optionSlot(option)?.value === true
            ).length;

            if (
                (this.props.min && n < this.props.min) ||
                (this.props.max && n > this.props.max)
            ) {
                return false;
            }
        }

        if (this.props.required) {
            return findFirst(
                this.props.options,
                (option: IOption) =>
                    this.valueOf<boolean>(option.id)?.value === true
            )
                ? true
                : false;
        }

        return true;
    }
}

// As long as the typings for Intl are incomplete, we need to declare them ourselves.
// See https://github.com/microsoft/TypeScript/issues/46907
// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Intl {
    class ListFormat {
        constructor(
            locales?: string | string[],
            options?: {
                type?: "conjunction" | "disjunction";
            }
        );
        format(values: string[]): string;
    }
}
