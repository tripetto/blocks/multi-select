/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, Slot, condition, tripetto } from "@tripetto/runner";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: `${PACKAGE_NAME}:none`,
})
export class NoneCondition extends ConditionBlock {
    @condition
    noneSelected(): boolean {
        const slots = this.slots;

        return (
            !slots ||
            !slots.each((slot: Slot) => {
                const option = this.valueOf<boolean>(slot);

                return (option && option.value) || false;
            })
        );
    }
}
