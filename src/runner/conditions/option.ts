/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, condition, tripetto } from "@tripetto/runner";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
})
export class OptionCondition extends ConditionBlock<{
    readonly selected: boolean;
}> {
    @condition
    isSelected(): boolean {
        const optionSlot = this.valueOf<boolean>();

        return (
            (optionSlot && optionSlot.value === this.props.selected) || false
        );
    }
}
