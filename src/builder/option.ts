/** Dependencies */
import {
    Collection,
    Forms,
    Markdown,
    Slots,
    affects,
    alias,
    created,
    definition,
    deleted,
    editor,
    insertVariable,
    isBoolean,
    isString,
    markdownifyToString,
    name,
    pgettext,
    refreshed,
    renamed,
    reordered,
    score,
} from "@tripetto/builder";
import { MultiSelect } from "./index";
import { IOption } from "../runner";

export class Option extends Collection.Item<MultiSelect> implements IOption {
    @definition("string")
    @name
    name = "";

    @definition("string", "optional")
    description?: string;

    @definition("string", "optional")
    @alias
    value?: string;

    @definition("string", "optional")
    @affects("#refresh")
    labelForTrue?: string;

    @definition("string", "optional")
    @affects("#refresh")
    labelForFalse?: string;

    @definition("number", "optional")
    @score
    score?: number;

    @definition("boolean", "optional")
    @affects("#name")
    exclusive?: boolean;

    @created
    @reordered
    @renamed
    @refreshed
    defineSlot(): void {
        const optionName =
            (this.name &&
                markdownifyToString(
                    this.name,
                    Markdown.MarkdownFeatures.Formatting |
                        Markdown.MarkdownFeatures.Hyperlinks
                )) ||
            undefined;

        const slot = this.ref.slots.dynamic({
            type: Slots.Boolean,
            reference: this.id,
            label: pgettext("block:multi-select", "Option"),
            sequence: this.index,
            name: optionName,
            alias: this.value,
            required: this.ref.required,
            exportable:
                this.ref.format !== "concatenate" && this.ref.exportable,
            pipeable: {
                label: pgettext("block:multi-select", "Option"),
                content:
                    this.name !== optionName
                        ? {
                              string: optionName || "",
                              markdown: this.name,
                          }
                        : "name",
                alias: this.ref.alias,
                legacy: "Option",
            },
        });

        slot.labelForTrue =
            this.labelForTrue ||
            this.ref.labelForTrue ||
            pgettext("block:multi-select", "Selected");

        slot.labelForFalse =
            this.labelForFalse ||
            this.ref.labelForFalse ||
            pgettext("block:multi-select", "Not selected");
    }

    @deleted
    deleteSlot(): void {
        this.ref.slots.delete(this.id, "dynamic");
    }

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:multi-select", "Name"),
            form: {
                title: pgettext("block:multi-select", "Option name"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .action("@", insertVariable(this))
                        .autoFocus()
                        .autoSelect()
                        .enter(this.editor.close)
                        .escape(this.editor.close),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:multi-select", "Description"),
            form: {
                title: pgettext("block:multi-select", "Description"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "description", undefined)
                    ).action("@", insertVariable(this)),
                ],
            },
            activated: isString(this.description),
        });

        this.editor.group(pgettext("block:multi-select", "Options"));

        this.editor.option({
            name: pgettext("block:multi-select", "Exclusivity"),
            form: {
                title: pgettext("block:multi-select", "Exclusivity"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:multi-select",
                            "Unselect all other options when selected"
                        ),
                        Forms.Checkbox.bind(this, "exclusive", undefined, true)
                    ),
                ],
            },
            activated: isBoolean(this.exclusive),
        });

        const defaultLabelForTrue = pgettext("block:multi-select", "Selected");
        const defaultLabelForFalse = pgettext(
            "block:multi-select",
            "Not selected"
        );

        this.editor.option({
            name: pgettext("block:multi-select", "Labels"),
            form: {
                title: pgettext("block:multi-select", "Labels"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForTrue", undefined)
                    ).placeholder(defaultLabelForTrue),
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForFalse", undefined)
                    ).placeholder(defaultLabelForFalse),
                    new Forms.Static(
                        pgettext(
                            "block:multi-select",
                            "These labels will be used in the dataset and override the default values %1 and %2.",
                            `**${defaultLabelForTrue}**`,
                            `**${defaultLabelForFalse}**`
                        )
                    ).markdown(),
                ],
            },
            activated:
                isString(this.labelForTrue) || isString(this.labelForFalse),
        });

        this.editor.option({
            name: pgettext("block:multi-select", "Identifier"),
            form: {
                title: pgettext("block:multi-select", "Identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:multi-select",
                            "If an option identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
        });

        const scoreSlot = this.ref.slots.select<Slots.Numeric>(
            "score",
            "feature"
        );

        this.editor.option({
            name: pgettext("block:multi-select", "Score"),
            form: {
                title: pgettext("block:multi-select", "Score"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "score", undefined)
                    )
                        .precision(scoreSlot?.precision || 0)
                        .digits(scoreSlot?.digits || 0)
                        .decimalSign(scoreSlot?.decimal || "")
                        .thousands(
                            scoreSlot?.separator ? true : false,
                            scoreSlot?.separator || ""
                        )
                        .prefix(scoreSlot?.prefix || "")
                        .prefixPlural(scoreSlot?.prefixPlural || undefined)
                        .suffix(scoreSlot?.suffix || "")
                        .suffixPlural(scoreSlot?.suffixPlural || undefined),
                ],
            },
            activated: true,
            locked: scoreSlot ? true : false,
            disabled: scoreSlot ? false : true,
        });
    }
}
