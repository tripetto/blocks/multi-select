/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    collection,
    definition,
    editor,
    markdownifyToString,
    pgettext,
    tripetto,
} from "@tripetto/builder";
import { Option } from "../option";

/** Assets */
import ICON from "../../../assets/condition.svg";
import ICON_SELECTED from "../../../assets/selected.svg";
import ICON_NOTSELECTED from "../../../assets/notselected.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:multi-select", "Option");
    },
})
export class OptionCondition extends ConditionBlock {
    @affects("#name")
    @collection("#options")
    option: Option | undefined;

    @affects("#icon")
    @definition
    selected = true;

    get icon() {
        return this.selected ? ICON_SELECTED : ICON_NOTSELECTED;
    }

    get name() {
        return markdownifyToString(this.option?.name || "") || this.type.label;
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            controls: [
                new Forms.Checkbox(
                    markdownifyToString(this.option?.name || "") ||
                        pgettext("block:multi-select", "Option is selected"),
                    Forms.Checkbox.bind(this, "selected", true)
                ),
            ],
        });
    }
}
