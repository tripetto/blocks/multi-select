/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import { ConditionBlock, pgettext, tripetto } from "@tripetto/builder";

/** Assets */
import ICON_NOTSELECTED from "../../../assets/notselected.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:none`,
    version: PACKAGE_VERSION,
    icon: ICON_NOTSELECTED,
    get label() {
        return pgettext("block:multi-select", "No options selected");
    },
})
export class NoneCondition extends ConditionBlock {}
