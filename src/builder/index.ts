/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    Markdown,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    isBoolean,
    isNumber,
    isString,
    npgettext,
    pgettext,
    slots,
    tripetto,
} from "@tripetto/builder";
import { Option } from "./option";
import { OptionCondition } from "./conditions/option";
import { NoneCondition } from "./conditions/none";
import { ScoreCondition } from "./conditions/score";
import { TScoreModes } from "../runner/conditions/score";
import { CounterCondition } from "./conditions/counter";
import { TCounterModes } from "../runner/conditions/counter";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_SELECTED from "../../assets/selected.svg";
import ICON_COUNTER from "../../assets/counter.svg";
import ICON_SCORE from "../../assets/score.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:multi-select", "Dropdown (multi-select)");
    },
})
export class MultiSelect extends NodeBlock {
    @definition("items")
    @affects("#name")
    readonly options = Collection.of<Option, MultiSelect>(Option, this);

    @definition("boolean", "optional")
    @affects("#required")
    @affects("#collection", "options")
    required?: boolean;

    @definition("number", "optional")
    min?: number;

    @definition("number", "optional")
    max?: number;

    @definition("string", "optional")
    @affects("#label")
    @affects("#slots")
    @affects("#collection", "options")
    alias?: string;

    @definition("boolean", "optional")
    @affects("#slots")
    @affects("#collection", "options")
    exportable?: boolean;

    @definition("string", "optional")
    @affects("#collection", "options")
    labelForTrue?: string;

    @definition("string", "optional")
    @affects("#collection", "options")
    labelForFalse?: string;

    @definition("boolean", "optional")
    randomize?: boolean;

    @definition("string", "optional")
    @affects("#slots")
    @affects("#collection", "options")
    format?: "fields" | "concatenate" | "both";

    @definition("string", "optional")
    formatSeparator?:
        | "comma"
        | "space"
        | "list"
        | "bullets"
        | "numbers"
        | "conjunction"
        | "disjunction"
        | "custom";

    @definition("string", "optional")
    formatSeparatorCustom?: string;

    get label() {
        return npgettext(
            "block:multi-select",
            "%2 (%1 option)",
            "%2 (%1 options)",
            this.options.count,
            this.type.label
        );
    }

    @slots
    defineSlot(): void {
        this.slots.meta({
            type: Slots.Number,
            reference: "counter",
            label: pgettext("block:multi-select", "Counter"),
            exportable: false,
            protected: true,
        });

        if (this.format === "concatenate" || this.format === "both") {
            this.slots.feature({
                type: Slots.Text,
                reference: "concatenation",
                label: pgettext("block:multi-select", "Text value"),
                exportable: this.exportable,
                alias: this.alias,
                protected: true,
            });
        } else {
            this.slots.delete("concatenation", "feature");
        }
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        const collection = this.editor.collection({
            collection: this.options,
            title: pgettext("block:multi-select", "Options"),
            icon: ICON_SELECTED,
            placeholder: pgettext("block:multi-select", "Unnamed option"),
            sorting: "manual",
            autoOpen: true,
            allowVariables: true,
            allowImport: true,
            allowExport: true,
            allowDedupe: true,
            showAliases: true,
            markdown:
                Markdown.MarkdownFeatures.Formatting |
                Markdown.MarkdownFeatures.Hyperlinks,
            indicator: (option) =>
                (option.exclusive &&
                    pgettext(
                        "block:multi-select",
                        "Exclusive"
                    ).toUpperCase()) ||
                undefined,
            emptyMessage: pgettext(
                "block:multi-select",
                "Click the + button to add an option..."
            ),
        });

        this.editor.groups.settings();

        const min = new Forms.Numeric(
            Forms.Numeric.bind(this, "min", undefined)
        )
            .min(1)
            .max(this.max)
            .visible(isNumber(this.min))
            .indent(32)
            .width(75)
            .on(() => {
                max.min(this.min || 1);
            });
        const max = new Forms.Numeric(
            Forms.Numeric.bind(this, "max", undefined)
        )
            .min(this.min || 1)
            .visible(isNumber(this.max))
            .indent(32)
            .width(75)
            .on(() => {
                min.max(this.max);
            });

        this.editor.option({
            name: pgettext("block:multi-select", "Limits"),
            form: {
                title: pgettext("block:multi-select", "Limits"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:multi-select",
                            "Minimum number of selected options"
                        ),
                        isNumber(this.min)
                    ).on((c) => {
                        min.visible(c.isChecked);
                    }),
                    min,
                    new Forms.Checkbox(
                        pgettext(
                            "block:multi-select",
                            "Maximum number of selected options"
                        ),
                        isNumber(this.max)
                    ).on((c) => {
                        max.visible(c.isChecked);
                    }),
                    max,
                ],
            },
            activated: isNumber(this.max) || isNumber(this.min),
        });

        this.editor.option({
            name: pgettext("block:multi-select", "Randomization"),
            form: {
                title: pgettext("block:multi-select", "Randomization"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:multi-select",
                            "Randomize the options (using [Fisher-Yates shuffle](%1))",
                            "https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle"
                        ),
                        Forms.Checkbox.bind(this, "randomize", undefined, true)
                    ).markdown(),
                ],
            },
            activated: isBoolean(this.randomize),
        });

        this.editor.groups.options();
        this.editor.required(this);
        this.editor.visibility();

        const defaultLabelForTrue = pgettext("block:multi-select", "Selected");
        const defaultLabelForFalse = pgettext(
            "block:multi-select",
            "Not selected"
        );

        this.editor.option({
            name: pgettext("block:multi-select", "Labels"),
            form: {
                title: pgettext("block:multi-select", "Labels"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForTrue", undefined)
                    ).placeholder(defaultLabelForTrue),
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForFalse", undefined)
                    ).placeholder(defaultLabelForFalse),
                    new Forms.Static(
                        pgettext(
                            "block:multi-select",
                            "These labels will be used in the dataset and override the default values %1 and %2.",
                            `**${defaultLabelForTrue}**`,
                            `**${defaultLabelForFalse}**`
                        )
                    ).markdown(),
                ],
            },
            activated:
                isString(this.labelForTrue) || isString(this.labelForFalse),
        });

        this.editor.scores({
            target: this,
            collection,
            description: pgettext(
                "block:multi-select",
                "Generates a score based on the selected options. Open the settings panel for each option to set the individual score for that option."
            ),
        });

        const alias = this.editor
            .alias(this)
            .disabled(this.format !== "concatenate" && this.format !== "both");

        const formatSeparatorCustom = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "formatSeparatorCustom", undefined)
        )
            .visible(this.formatSeparator === "custom")
            .sanitize(false)
            .width(200)
            .label(pgettext("block:multi-select", "Use this separator:"));

        const formatSeparatorOptions = new Forms.Group([
            new Forms.Dropdown<
                | "comma"
                | "space"
                | "list"
                | "bullets"
                | "numbers"
                | "conjunction"
                | "disjunction"
                | "custom"
            >(
                [
                    {
                        label: pgettext(
                            "block:multi-select",
                            "Comma separated"
                        ),
                        value: "comma",
                    },
                    {
                        label: pgettext(
                            "block:multi-select",
                            "Space separated"
                        ),
                        value: "space",
                    },
                    {
                        label: pgettext(
                            "block:multi-select",
                            "List on multiple lines"
                        ),
                        value: "list",
                    },
                    {
                        label: pgettext("block:multi-select", "Bulleted list"),
                        value: "bullets",
                    },
                    {
                        label: pgettext("block:multi-select", "Numbered list"),
                        value: "numbers",
                    },
                    {
                        label: pgettext(
                            "block:multi-select",
                            "Language sensitive conjunction (_, _, and _)"
                        ),
                        value: "conjunction",
                    },
                    {
                        label: pgettext(
                            "block:multi-select",
                            "Language sensitive disjunction (_, _, or _)"
                        ),
                        value: "disjunction",
                    },
                    {
                        label: pgettext(
                            "block:multi-select",
                            "Custom separator"
                        ),
                        value: "custom",
                    },
                ],
                Forms.Radiobutton.bind(
                    this,
                    "formatSeparator",
                    undefined,
                    "comma"
                )
            )
                .label(
                    pgettext(
                        "block:multi-select",
                        "How to separate the selected options:"
                    )
                )
                .on((formatSeparator) => {
                    formatSeparatorCustom.visible(
                        formatSeparator.value === "custom"
                    );
                }),
            formatSeparatorCustom,
        ]).visible(this.format === "concatenate" || this.format === "both");

        this.editor.option({
            name: pgettext("block:multi-select", "Data format"),
            form: {
                title: pgettext("block:multi-select", "Data format"),
                controls: [
                    new Forms.Radiobutton<"fields" | "concatenate" | "both">(
                        [
                            {
                                label: pgettext(
                                    "block:multi-select",
                                    "Every option as a separate field"
                                ),
                                description: pgettext(
                                    "block:multi-select",
                                    "Every option is included in the dataset as a separate value."
                                ),
                                value: "fields",
                            },
                            {
                                label: pgettext(
                                    "block:multi-select",
                                    "Text field with a list of all selected options"
                                ),
                                description: pgettext(
                                    "block:multi-select",
                                    "All the selected options are concatenated to a single string of text separated using a configurable separator."
                                ),
                                value: "concatenate",
                            },
                            {
                                label: pgettext(
                                    "block:multi-select",
                                    "Both options above"
                                ),
                                description: pgettext(
                                    "block:multi-select",
                                    "Includes every option in the dataset together with the concatenated text."
                                ),
                                value: "both",
                            },
                        ],
                        Forms.Radiobutton.bind(
                            this,
                            "format",
                            undefined,
                            "fields"
                        )
                    )
                        .label(
                            pgettext(
                                "block:multi-select",
                                "This setting determines how the data is stored in the dataset:"
                            )
                        )
                        .on((format) => {
                            formatSeparatorOptions.visible(
                                format.value === "concatenate" ||
                                    format.value === "both"
                            );

                            alias.disabled(
                                this.format !== "concatenate" &&
                                    this.format !== "both"
                            );
                        }),
                    formatSeparatorOptions,
                ],
            },
            activated: isString(this.format),
        });

        this.editor.exportable(this);
    }

    @conditions
    defineConditions(): void {
        this.options.each((option: Option) => {
            if (option.name) {
                this.conditions.template({
                    condition: OptionCondition,
                    markdown: option.name,
                    icon: ICON_SELECTED,
                    burst: true,
                    props: {
                        slot: this.slots.select(option.id),
                        option,
                        selected: true,
                    },
                });
            }
        });

        if (this.options.count > 0) {
            this.conditions.template({
                condition: NoneCondition,
                separator: true,
            });
        }

        const counter = this.slots.select("counter", "meta");

        if (counter && counter.label) {
            const group = this.conditions.group(counter.label, ICON_COUNTER);

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext(
                            "block:multi-select",
                            "Counter is equal to"
                        ),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:multi-select",
                            "Counter is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:multi-select",
                            "Counter is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:multi-select",
                            "Counter is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext(
                            "block:multi-select",
                            "Counter is between"
                        ),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:multi-select",
                            "Counter is not between"
                        ),
                    },
                ],
                (condition: { mode: TCounterModes; label: string }) => {
                    group.template({
                        condition: CounterCondition,
                        label: condition.label,
                        autoOpen: true,
                        props: {
                            slot: counter,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }

        const score = this.slots.select("score", "feature");

        if (score && score.label) {
            const group = this.conditions.group(score.label, ICON_SCORE);

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext(
                            "block:multi-select",
                            "Score is equal to"
                        ),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:multi-select",
                            "Score is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:multi-select",
                            "Score is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:multi-select",
                            "Score is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext(
                            "block:multi-select",
                            "Score is between"
                        ),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:multi-select",
                            "Score is not between"
                        ),
                    },
                    {
                        mode: "defined",
                        label: pgettext(
                            "block:multi-select",
                            "Score is calculated"
                        ),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:multi-select",
                            "Score is not calculated"
                        ),
                    },
                ],
                (condition: { mode: TScoreModes; label: string }) => {
                    group.template({
                        condition: ScoreCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: score,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }
    }
}
